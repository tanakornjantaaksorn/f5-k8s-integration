![alt text](img/diagram.png)

# IP & Network preparation
* **Network & vlan summary**

| Network        |   Description                              |
|----------------|--------------------------------------------|
| 172.16.23.0/24 |  K8s-cluster1, Peer-BGP, F5-virtual server |
| 172.16.24.0/24 |  K8s-cluster2, Peer-BGP, F5-virtual server |
| 172.16.11.0/24 |  F5-MGMT-IP                                |
| 172.32.0.0/16  |  K8s-cluster-IP                            |

<br>

| vlan   |   Description                              |
|--------|--------------------------------------------|
| vlan23 |  K8s-cluster1, Peer-BGP, F5-virtual server |
| vlan24 |  K8s-cluster2, Peer-BGP, F5-virtual server |
| vlan11 |  F5-MGMT-IP                                |

<br>

* **K8s Cluster 1**

| Network          |   Description     |
|------------------|-------------------|
| 172.16.23.100/24 |  K8s-master-01-c1 |
| 172.16.23.101/24 |  K8s-worker-01-c1 |
| 172.16.23.102/24 |  K8s-worker-02-c1 |
| 172.32.0.0/16    |  K8s-cluster-IP   |

<br>

* **K8s Cluster 2**

| Network          |   Description     |
|------------------|-------------------|
| 172.16.24.100/24 |  K8s-master-01-c2 |
| 172.16.24.101/24 |  K8s-worker-01-c2 |
| 172.16.24.102/24 |  K8s-worker-02-c2 |
| 172.32.0.0/16    |  K8s-cluster-IP   |

<br>

* **F5 VE**

| Network           |   Description           |
|-------------------|-------------------------|
| 172.16.23.12%0/24 |  Self-ip Route Domain 0 |
| 172.16.24.11%2/24 |  Self-ip Route Domain 2 |
| 172.16.11.89/24   |  F5-ip MGMT             |
| 172.16.23.200/24  |  F5-ip virtual server   |
| 172.16.24.200/24  |  F5-ip virtual server   |

<br>

# Config F5 network interface

![alt text](img/interface.png)

* **Install the AS3 Package**

The first task is to download the latest RPM file.
```
https://github.com/F5Networks/f5-appsvcs-extension/releases/download/v3.25.0/f5-appsvcs-3.25.0-3.noarch.rpm
```
Open BIG-IP utility click iAPPs > Package Management LX > Import
![alt text](img/install-as3-1.png)

Click Choose File and then browse to the location you saved the RPM file. Click the Upload button.
![alt text](img/install-as3-2.png)

Continue with Checking for a successful installation.
![alt text](img/install-as3-3.png)

# Config VLAN on F5-VE

* On BIG-IP utility click  Network > VLANs

1. Name: VLAN23
2. Tag: VLAN23
3. Interface: 1.1
4. Tagging: Untagged
5. click add

![alt text](img/add-vlan-1.png)

Verify interface in text box, click Finished.
![alt text](img/add-vlan-2.png)

Config VLAN 24 same VLAN23 but edit interface: 1.2 and tag: 24
![alt text](img/add-vlan-3.png)

# Config Route Domains

On BIG-IP utility click  Network > Route Domains add vlan23 to member and enabled BGP of route domain 0, Click update button.
![alt text](img/route-domain-1.png)

Create Route Domain 2 add vlan24 to member and enable BGP, Click Update.
![alt text](img/route-domain-2.png)

# Config Self-IPs F5

On BIG-IP utility click  Network > Self IPs > create

1. Name: self-peer-bgp-c1
2. IP Address: 172.16.23.12
3. Netmask: 255.255.255.0
4. VLAN: VLAN23
5. Port Lockdown: Allow All
6. Click Finished or update

![alt text](img/self-ip-1.png)

Config self-IP for peer bgp cluster2

1. Name: self-peer-bgp-c2
2. IP Address: 172.16.24.11%2
3. Netmask: 255.255.255.0
4. VLAN: VLAN24
5. Port Lockdown: Allow All
6. Click Finished or update

![alt text](img/self-ip-2.png)

# Config default Routes F5

On BIG-IP utility click  Network > Routes > add

1. Name: default-c1
2. Destination: 0.0.0.0
3. Netmask: 0.0.0.0
4. Resource: Use Gateway
5. Gateway Address: 172.16.23.1
6. MTU: 1500
7. Click Finished or update

![alt text](img/default-route-1.png)

On BIG-IP utility click  Network > Routes > add

1. Name: default-c2
2. Destination: 0.0.0.0
3. Netmask: 0.0.0.0
4. Resource: Use Gateway
5. Gateway Address: 172.16.24.1%2
6. MTU: 1500
7. Click Finished or update

![alt text](img/default-route-2.png)

# Config BGP routing in F5

Open a new terminal and ssh to BIG-IP
```
$ ssh admin@<F5-MGMT-IP>
```

Access the IMI shell
```
# imish
```

Switch to enable mode
```
[0]> enable
```

Enter configuration mode
```
[0]# config terminal
```

Setup route bgp with AS Number 64512
```
[0]# router bgp 64512
```

Create BGP Peer group
```
[0]# router calico-k8s peer-group
```

assign peer group as BGP neighbors
```
[0]# neighbor calico-k8s remote-as 64512
```

we need to add all the peers: the other BIG-IP, our k8s cluster 1 components
```
[0]# neighbor 172.16.23.100 peer-group calico-k8s
[0]# neighbor 172.16.23.101 peer-group calico-k8s
[0]# neighbor 172.16.23.102 peer-group calico-k8s
```

save configuration
```
[0]# write
```

exit
```
[0]# end
```

show config
```
[0]# show running-config
```

![alt text](img/showrun-1.png)

# Config BGP route domain 2

Access the IMI shell route domain 2
```
# imish -r 2
```

Switch to enable mode
```
[2]> enable
```

Enter configuration mode
```
[2]# config terminal
```

Setup route bgp with AS Number 64513
```
[2]# router bgp 64513
```

Create BGP Peer group
```
[2]# router calico-k8s peer-group
```

assign peer group as BGP neighbors
```
[2]# neighbor calico-k8s remote-as 64513
```

we need to add all the peers: the other BIG-IP, our k8s cluster 2 components
```
[2]# neighbor 172.16.24.100 peer-group calico-k8s
[2]# neighbor 172.16.24.101 peer-group calico-k8s
[2]# neighbor 172.16.24.102 peer-group calico-k8s
```

save configuration
```
[2]# write
```

exit
```
[2]# end
```

show config
```
[2]# show running-config
```

![alt text](img/showrun-2.png)

# K8s cluster 1 config BGP route

Install calicoctl, retrieve the calicoctl binary
```
K8s-master-01# curl -O -L https://github.com/projectcalico/calicoctl/releases/download/v3.15.1/calicoctl
K8s-master-01# chmod +x calicoctl
K8s-master-01# sudo mv calicoctl /usr/local/bin
```

Copy the the calicoctl.cfg file to /etc/calico/
```yaml
apiVersion: projectcalico.org/v3
kind: CalicoAPIConfig
metadata:
spec:
  datastoreType: "kubernetes"
  kubeconfig: "/home/ubuntu/.kube/config"
```

Create directory and copy file to /etc/calico
```
K8s-master-01# sudo mkdir /etc/calico
K8s-master-01# sudo cp calicoctl.cfg /etc/calico/
```

Verify calicoctl is properly set up
```
K8s-master-01# calicoctl get nodes
```

![alt text](img/calico-get-node.png)

Set up the Calico BGP config in file caliconf.yaml
```yaml
apiVersion: projectcalico.org/v3
kind: BGPConfiguration
metadata:
  name: default
spec:
  logSeverityScreen: Info
  nodeToNodeMeshEnabled: true
  asNumber: 64512
```

Set up the BIG-IP BGP peer calipeer.yaml
```yaml
apiVersion: projectcalico.org/v3
kind: BGPPeer
metadata:
  name: bgppeer-global-bigip1
spec:
  peerIP: 172.16.23.12
  asNumber: 64512
```

```
K8s-master-01# calicoctl create -f calipeer.yaml
```

# K8s cluster 2 config BGP route

Set up the Calico BGP config in file caliconf.yaml
```yaml
apiVersion: projectcalico.org/v3
kind: BGPConfiguration
metadata:
  name: default
spec:
  logSeverityScreen: Info
  nodeToNodeMeshEnabled: true
  asNumber: 64513
```

Set up the BIG-IP BGP peer calipeer.yaml
```yaml
apiVersion: projectcalico.org/v3
kind: BGPPeer
metadata:
  name: bgppeer-global-bigip1
spec:
  peerIP: 172.16.24.11
  asNumber: 64513
```

```
K8s-master-01# calicoctl create -f calipeer.yaml
```

# Show route BGP

show routing on route domain 0
![alt text](img/show-route-1.png)

show routing on route domain 2
![alt text](img/show-route-2.png)

# BIG-IP Setup configmap deployment

Create partition that will be used by F5 Container Ingress Service.

1. Goto: System > Users > Partition List
2. Create a new partition called “k8s-c1”
3. Click Finished

![alt text](img/partition-1.png)

create a “secret”, “serviceaccount”, and “clusterrolebinding”.
```
K8s-master-01# kubectl create secret generic bigip-login -n kube-system --from-literal=username=admin --from-literal=password=<password>
K8s-master-01# kubectl create serviceaccount k8s-bigip-ctlr -n kube-system
K8s-master-01# kubectl create clusterrolebinding k8s-bigip-ctlr-clusteradmin --clusterrole=cluster-admin --serviceaccount=kube-system:k8s-bigip-ctlr
```

CIS deployment will start the f5-k8s-controller container on one of the worker nodes. Config file cluster-deployment.yaml
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: k8s-bigip-ctlr
  namespace: kube-system
spec:
  replicas: 1
  selector:
    matchLabels:
      app: k8s-bigip-ctlr
  template:
    metadata:
      name: k8s-bigip-ctlr
      labels:
        app: k8s-bigip-ctlr
    spec:
      serviceAccountName: k8s-bigip-ctlr
      containers:
        - name: k8s-bigip-ctlr
          image: "f5networks/k8s-bigip-ctlr"
          imagePullPolicy: IfNotPresent
          env:
            - name: BIGIP_USERNAME
              valueFrom:
                secretKeyRef:
                  name: bigip-login
                  key: username
            - name: BIGIP_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: bigip-login
                  key: password
          command: ["/app/bin/k8s-bigip-ctlr"]
          args: [
            "--bigip-username=$(BIGIP_USERNAME)",
            "--bigip-password=$(BIGIP_PASSWORD)",
            "--bigip-url=https://< F5 MGMT-IP >",
            "--insecure=true",
            "--bigip-partition=k8s-c1",
            "--pool-member-type=cluster",
            "--log-level=DEBUG",
            "--log-as3-response=true",
          ]
```

Create the CIS deployment with command
```
K8s-master-01# kubectl apply -f cluster-deployment.yaml
```

Verify the deployment
```
K8s-master-01# kubectl get deployment k8s-bigip-ctlr -n kube-system
```

![alt text](img/CIS-1.png)

To locate on which node CIS is running, you can use the command
```
K8s-master-01# kubectl get pods -o wide -n kube-system
```

![alt text](img/CIS-2.png)

# Deploy appication

Create app deployment file called deployment-hello-world.yaml
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: f5-hello-world-web
  namespace: default
spec:
  replicas: 5
  selector:
    matchLabels:
      app: f5-hello-world-web
  template:
    metadata:
      labels:
        app: f5-hello-world-web
    spec:
      containers:
      - env:
        - name: service_name
          value: f5-hello-world-web
        image: f5devcentral/f5-hello-world:latest
        imagePullPolicy: IfNotPresent
        name: f5-hello-world-web
        ports:
        - containerPort: 8080
          protocol: TCP
```

Create a file called clusterip-service-hello-world.yaml
```yaml
apiVersion: v1
kind: Service
metadata:
  name: f5-hello-world-web
  namespace: default
  labels:
    app: f5-hello-world-web
    cis.f5.com/as3-tenant: bgp1-app1
    cis.f5.com/as3-app: app1
    cis.f5.com/as3-pool: bgp1_pool1
spec:
  ports:
  - name: f5-hello-world-web
    port: 8080
    protocol: TCP
    targetPort: 8080
  type: ClusterIP
  selector:
    app: f5-hello-world-web
```

Create a file called configmap-hello-world.yaml
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: f5-as3-declaration
  namespace: default
  labels:
    f5type: virtual-server
    as3: "true"
data:
  template: |
    {
        "class": "AS3",
        "declaration": {
            "class": "ADC",
            "schemaVersion": "3.25.0",
            "id": "urn:uuid:33045210-3ab8-4636-9b2a-c98d22ab915d",
            "label": "http",
            "remark": "app1 example",
            "bgp1-app1": {
                "class": "Tenant",
                "defaultRouteDomain": 0,
                "app1": {
                    "class": "Application",
                    "template": "http",
                    "serviceMain": {
                        "class": "Service_HTTP",
                        "virtualAddresses": [
                            "172.16.23.200"
                        ],
                        "pool": "bgp1_pool1",
                        "virtualPort": 80
                    },
                    "bgp1_pool1": {
                        "class": "Pool",
                        "monitors": [
                            "http"
                        ],
                        "members": [
                            {
                                "servicePort": 8080,
                                "serverAddresses": []
                            }
                        ]
                    }
                }
            }
        }
    }
```

We can launch our application
```
K8s-master-01# kubectl apply -f deployment-hello-world.yaml
K8s-master-01# kubectl apply -f clusterip-service-hello-world.yaml
K8s-master-01# kubectl apply -f configmap-hello-world.yaml
```

To check the status of our deployment, you can run the command
```
K8s-master-01# kubectl get pods -o wide
```
![alt text](img/app-1.png)

```
K8s-master-01# kubectl describe svc f5-hello-world
```
![alt text](img/app-2.png)

**We can check the configuration on bigip.**

* GoTo: Local Traffic > Virtual Server
* Here you can see a new Virtual Server, “serviceMain” was created, listening on 172.16.23.200:80 in partition “bgp1-app1”

![alt text](img/app-3.png)

* Check the Pools to see a new pool and the associated pool members.
* GoTo: Local Traffic > Pools and select the “bgp1_pool” pool.

![alt text](img/app-4.png)

Click the Members tab.

![alt text](img/app-5.png)

Access you web application via browser

![alt text](img/app-6.png)